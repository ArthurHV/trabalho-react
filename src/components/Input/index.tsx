import React from 'react';
import { View, Text, TextInput, StyleSheet } from 'react-native';

export default function Input({param}){
    return(
        <View style={styles.box}>
            <TextInput style={styles.textinside} placeholder={param}/>
        </View>
    )
}

const styles=StyleSheet.create({
    box:{
        backgroundColor:'#C6B9CD',
        width:368,
        height:47,
        fontSize:18,
        color:"black",
        borderRadius:8,
        marginLeft:18,
        marginTop:11,
    },

    textinside:{
        color:"black",
        marginLeft:18,
        marginTop:15,
    }
})
