import React from "react";
import {Text, TouchableOpacity, View, StyleSheet} from "react-native";

const RegisterButton = ()=> {



    return(
        <TouchableOpacity>
            <View style={styles.button}>
                <Text style={styles.texto}>Cadastrar-se</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles=StyleSheet.create({

    button:{
        width:264,
        height:44,
        backgroundColor:'#363637',
        borderRadius:10,
        alignItems:"center",
        justifyContent:"center",
        marginLeft:70,
        marginTop:20,
    },

    texto:{
        fontSize:20,
        color:'white',
        fontWeight:"700",
        marginBottom:5,
    }
})

export default RegisterButton;