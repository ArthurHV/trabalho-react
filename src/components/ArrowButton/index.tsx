import React from "react";
import {TouchableOpacity, View, StyleSheet, Image} from "react-native";

const ArrowButton = ()=> {



    return(
        <TouchableOpacity>
            <Image source={require('../../../assets/icone-seta.png')} style={styles.button}/>
        </TouchableOpacity>
    )
}

const styles=StyleSheet.create({

    button:{
        width:37,
        height:38,   
        marginLeft:26,
        marginTop:23,
    },

})

export default ArrowButton;