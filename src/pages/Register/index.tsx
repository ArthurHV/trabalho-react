import {View, Text, Image} from "react-native";
import { Container, Logo, Title, Titles, FinalText} from "./style";
import Input from '../../components/Input';
import ContinueButton from '../../components/ContinueButton';
import ArrowButton from '../../components/ArrowButton';

function RegisterSC() {
    return (
        <Container>
            <ArrowButton/>
            <Logo source={require('../../../assets/logo-reus-e.png')} />
            <Title>Dados Cadastrais</Title>
            <Titles>Nome completo:</Titles>
            <Input param={"Digite seu completo"} />
            <Titles>Nº de celular:</Titles>
            <Input param={"Digite seu nº de celular"} />
            <Titles>E-mail:</Titles>
            <Input param={"Digite seu endereço de e-mail"} />
            <Titles>Senha:</Titles>
            <Input param={"Digite sua senha"} />
            <Titles>Confirme sua senha:</Titles>
            <Input param={"Digite novamente sua senha"} />
            <ContinueButton/>
        </Container>
    )
}

export default RegisterSC;