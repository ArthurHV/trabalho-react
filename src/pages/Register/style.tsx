import styled from 'styled-components/native';

export const Container = styled.View`
    flex: 1;
    flex-direction:"column";
    align-content:"center";
    background: #D6D3F0;
    width: 428px;
    height: 926px;
`;

export const Logo = styled.Image`
    width: 180px;
    height: 45px;   
    margin-left: 128px;
`;

export const Title = styled.Text`
    color: black;
    font-size: 24px;
    margin-left: 108px;
    margin-top: 31px;
    font-weight: 700;
`

export const Titles = styled.Text`
    font-size: 20px;
    color: black;
    font-weight: 700;
    margin-left: 53px;
    margin-top: 40px;
`

export const FinalText = styled.Text`
    font-size: 14px;
    color: black;
    font-weight: 700;
    margin-left: 74px;
    margin-top: 49px;
`