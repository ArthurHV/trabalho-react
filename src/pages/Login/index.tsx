import {View, Text, Image} from "react-native";
import { Container, Icone, Title, Titles, FinalText} from "./style";
import ConfirmButton from '../../components/ConfirmButton';
import Input from '../../components/Input';
import RegisterButton from '../../components/RegisterButton';

function LoginSC() {
    return (
        <Container>
            <Icone source={require('../../../assets/icone-reus-e.png')} />
            <Title>Login</Title>
            <Titles>E-mail ou celular:</Titles>
            <Input param={"Digite seu e-mail ou nº de celular"} />
            <Titles>Senha:</Titles>
            <Input param={"Digite sua senha"} />
            <ConfirmButton />
            <FinalText>Não possui uma conta?</FinalText>
            <RegisterButton />
        </Container>
    )
}

export default LoginSC;